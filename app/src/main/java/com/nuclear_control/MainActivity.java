package com.nuclear_control;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

public class MainActivity extends Activity {
    private static final int REQUEST_CODE_ENABLE_DEVICE_ADMIN = 1;
    private static final int ACCESS_FINE_LOCATION = 2;
    private static final String PREFS_NAME = "nuclear_control_prefs";

    private TextView stateTextView;
    private Button grantPermissionsButton;
    private Button refreshStateButton;
    private Button registerUserButton;
    private Button disableActiveModeButton;
    private LocationManager locationManager;

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            processGeoMarker(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            processGeoMarker(locationManager.getLastKnownLocation(provider));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    enum State {
        NEEDS_PERMISSIONS,
        GRANTED_PERMISSIONS,
        PASSIVE_MODE,
        STRONG_MODE,
    }

    private void processGeoMarker(Location location) {
        if (location == null)
            return;

        final SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        double latitude = Double.longBitsToDouble(prefs.getLong("Latitude", Double.doubleToLongBits(0)));
        double longitude = Double.longBitsToDouble(prefs.getLong("Longitude", Double.doubleToLongBits(0)));

        if (latitude == 0 || longitude == 0) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            prefs.edit()
                    .putLong("Latitude", Double.doubleToRawLongBits(latitude))
                    .putLong("Longitude", Double.doubleToRawLongBits(longitude))
                    .apply();
        }

        Location geoMark = new Location("");
        geoMark.setLatitude(latitude);
        geoMark.setLongitude(longitude);

        if (location.distanceTo(geoMark) > 30) {
            toggleMode(false);
        } else {
            toggleMode(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stateTextView = findViewById(R.id.state_text_view);
        grantPermissionsButton = findViewById(R.id.grant_permissions_button);
        refreshStateButton = findViewById(R.id.refresh_state_button);
        registerUserButton = findViewById(R.id.enable_active_mode_button);
        disableActiveModeButton = findViewById(R.id.disable_active_mode_button);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        grantPermissionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions();
            }
        });

        refreshStateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshState();
            }
        });

        registerUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        disableActiveModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMode(false);
            }
        });

//        final String EXTRA_NAME = "de.blinkt.openvpn.api.profileName";
//        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
//        shortcutIntent.setClassName("de.blinkt.openvpn", "de.blinkt.openvpn.api.ConnectVPN");
//        shortcutIntent.putExtra(EXTRA_NAME, "upb ssl");
//        startActivity(shortcutIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    1000 * 5,
                    5,
                    locationListener
            );
//            locationManager.requestLocationUpdates(
//                    LocationManager.NETWORK_PROVIDER,
//                    1000 * 5,
//                    5,
//                    locationListener
//            );
        }

        refreshState();
    }

    private ComponentName componentName() {
        return new ComponentName(getApplicationContext(), AdminReceiver.class);
    }

    private void requestPermissions() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName());
        startActivityForResult(intent, REQUEST_CODE_ENABLE_DEVICE_ADMIN);

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
        }

        refreshState();
    }

    private void registerUser() {
        final SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        prefs.edit()
                .putLong("Latitude", Double.doubleToRawLongBits(0))
                .putLong("Longitude", Double.doubleToRawLongBits(0))
                .apply();
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, null);
        }
    }

    private void toggleMode(boolean isEnabled) {
        DevicePolicyManager policyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!policyManager.isAdminActive(componentName())) {
            Toast.makeText(this, "Сперва выдайте нужные разрешения.", Toast.LENGTH_LONG).show();
            refreshState();

            return;
        }
        policyManager.setCameraDisabled(componentName(), isEnabled);

        try {
            policyManager.setAlwaysOnVpnPackage(componentName(), "de.blinkt.openvpn", isEnabled);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("nuclear_control", "Установите OpenVPN для Android", e);
        }

        refreshState();
    }

    private void refreshState() {
        switch (getCurrentState()) {
            case NEEDS_PERMISSIONS:
                stateTextView.setText("Для работы приложения нужно получить разрешения.");
                grantPermissionsButton.setVisibility(View.VISIBLE);
                refreshStateButton.setVisibility(View.GONE);
                registerUserButton.setVisibility(View.GONE);
                disableActiveModeButton.setVisibility(View.GONE);
                break;
            case GRANTED_PERMISSIONS:
                stateTextView.setText("Для дальнейшей настройки приложения следуйте инструкции (adb shell dpm set-device-owner com.nuclear_control/.AdminReceiver).");
                grantPermissionsButton.setVisibility(View.GONE);
                refreshStateButton.setVisibility(View.VISIBLE);
                registerUserButton.setVisibility(View.GONE);
                disableActiveModeButton.setVisibility(View.GONE);
                break;
            case STRONG_MODE:
                stateTextView.setText("Приложение работает в строгом активном режиме, в этом режиме доступ в интернет предоставляется только через VPN, приложение нельзя удалить.");
                grantPermissionsButton.setVisibility(View.GONE);
                refreshStateButton.setVisibility(View.GONE);
                registerUserButton.setVisibility(View.GONE);
                disableActiveModeButton.setVisibility(View.GONE);
                break;
            case PASSIVE_MODE:
                stateTextView.setText("Теперь можно зарегистрировать сотрудника.");
                grantPermissionsButton.setVisibility(View.GONE);
                refreshStateButton.setVisibility(View.GONE);
                registerUserButton.setVisibility(View.VISIBLE);
                disableActiveModeButton.setVisibility(View.GONE);
                break;
        }
    }

    private State getCurrentState() {
        ComponentName componentName = componentName();
        DevicePolicyManager policyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

        if (!policyManager.isAdminActive(componentName)) {
            return State.NEEDS_PERMISSIONS;
        }
        if (!policyManager.isDeviceOwnerApp("com.nuclear_control")) {
            return State.GRANTED_PERMISSIONS;
        }

        if (!policyManager.getCameraDisabled(componentName)) {
            return State.PASSIVE_MODE;
        }

        policyManager.addUserRestriction(componentName, UserManager.DISALLOW_ADD_USER);
        policyManager.addUserRestriction(componentName, UserManager.DISALLOW_FACTORY_RESET);

        return State.STRONG_MODE;
    }
}

