package com.nuclear_control;

import android.app.admin.DeviceAdminReceiver;

/**
 * Trivial DeviceAdminReceiver used to identify this app's device administrator.
 */
public class AdminReceiver extends DeviceAdminReceiver {
}
