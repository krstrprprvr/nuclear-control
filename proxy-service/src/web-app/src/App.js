import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Logo from  './images/Logo.png'
import BackImg from  './images/background.png'
import LeftNavigation from './components/LeftNavigation/LeftNavigation';
import Top from './components/Top/Top';
import Main from './components/Main/Main';

class App extends Component {
  state = {
    main: "home"
  };


render() {
  const leftMenuLinks = [{
    name: "Сотрудники",
    id: "sotrudniki",
    onClickHandler: null
  }, {
    name: "Контроль",
    id: "control",
    onClickHandler: ()=>{this.setState({main: "control"});}
  }, {
    name: "Состояние",
    id: "sostoyanie",
    onClickHandler: ()=>{this.setState({main: "main"});}
  }, {
    name: "Аналитика",
    id: "analyze",
    onClickHandler: null
  }, {
    name: "Инструкции",
    id: "instruction",
    onClickHandler: null
  }
]; 

  return (

    <div id="main" >
      <div id="pageHeaderIcon"><img src={Logo} alt="Logo" /></div>
      <Top/>
      <Main page={this.state.main}/>
       <LeftNavigation links={leftMenuLinks}/>
      <footer id="pageFooter">
        <div>© Костыли и Велосипеды, 2019</div>
      </footer>
    </div>


    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}
}
export default App;
