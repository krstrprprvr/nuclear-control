import React, { useEffect } from 'react';
import classes from './MenuItem.css';

const menuItem = props => {
  return (
    <div className="MenuItem" onClick={props.onClick}>
        <span>{props.name}</span>
    </div>
  );
};

export default menuItem;