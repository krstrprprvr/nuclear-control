import React, { useEffect } from 'react';
import MenuItem from './MenuItem/MenuItem'
import './LeftNavigation.css';

const leftNavigation = props => {
  const links =  props.links.map((link, index) => {
    return (
        <MenuItem 
            name={link.name}
            key={link.id}
            onClick={link.onClickHandler}
        />
    );  
  });
    
  return (
    <nav id="mainNav">
        {links}
    </nav>);
};

export default leftNavigation;