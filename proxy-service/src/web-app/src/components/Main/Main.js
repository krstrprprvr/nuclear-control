import React, { useEffect } from 'react';
import './Main.css';
import Numb from '../../images/Number.png'
import Persent from '../../images/Persent.png'
import System from '../../images/System.png'
import Graphic from '../../images/Graphic.png'
import MenuItem from '../LeftNavigation/MenuItem/MenuItem'


const getMain = () => {
    return (
        <article id="mainArticle">
            <div id="mainOne"><img src={System} alt="System" /></div>
            <div id="mainTwo"><img src={Numb} alt="City" /></div>
            <div id="mainThree"><img src={Persent} alt="Persent" /></div>
            <div id="mainFour"><img src={Graphic} alt="Graphic" /></div>
        </article>
      );
}

const enableHandler = () => {
    // send request
}

const disableHandler = () => {
    // send request
}

const getControl = () => {
    return [<MenuItem name="Включить" onClick={enableHandler}/>, 
            <MenuItem name="Выключить" onClick={disableHandler}/>]
}

const main = props => {
    let content = null;
   if (props.page === 'main')  {
       content = getMain();
   } else if (props.page === 'control') {
       content = getControl()
   } else {
       content = getMain();
   }

   return (
        <article id="mainArticle">
            {content}
        </article>
   );
};

export default main;