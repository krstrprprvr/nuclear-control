import React, { useEffect } from 'react';
import './Top.css';
import City from '../../images/City.png'

const leftNavigation = props => {
  return (
    <header id="pageHeader">
        <div className="HeaderSearch" name="2"><input type="text" placeholder="Поиск..."/></div>
        <div className="HeaderUser" name="3"><span><img src={City} alt="City" /></span></div>
    </header>
  );
};

export default leftNavigation;