# How to install VPS with docker and configure VPN server 
Ubuntu 18.

== Docker ==
1. sudo apt update
2. sudo apt install apt-transport-https ca-certificates curl software-properties-common
3. curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
4. sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
5. apt-cache policy docker-ce
6. sudo apt install docker-ce
7. sudo systemctl status docker
8. docker run hello-world

== Docker compose ==
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

== Generate keys == 
docker-compose run --rm openvpn ovpn_genconfig -u udp://{vpn_server_address}
docker-compose run --rm openvpn ovpn_initpki

== RUN OpenVPN ==
docker-compose up -d openvpn

== Generate client cert ==
docker-compose run --rm openvpn easyrsa build-client-full {client_name} nopass 
docker-compose run --rm openvpn ovpn_getclient {client_name} > certificate.ovpn

== How to download it ==
scp apok@84.201.160.76:/home/apok/nuclear-control/proxy-service/certificate.ovpn .

== to build react app ==
docker build -t my-react-app .
============================